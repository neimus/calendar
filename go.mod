module gitlab.com/neimus/calendar

go 1.13

require (
	github.com/coreos/etcd v3.3.10+incompatible
	github.com/golang/protobuf v1.3.3
	github.com/jackc/pgx/v4 v4.6.0
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1
	go.uber.org/zap v1.10.0
	google.golang.org/grpc v1.28.0
)
