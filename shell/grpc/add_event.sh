#!/usr/bin/env bash

PATH_CURRENT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${PATH_CURRENT}/.color.sh
source ${PATH_CURRENT}/.parse_args.sh

SCRIPT_NAME=$(basename "${BASH_SOURCE[0]}")

#start=${start:-"2022-04-15T12:30:15.01Z"}
duration=${duration:-"3600"}
title=${title:-"Default event title"}
description=${description:-"Default event description"}
location=${location:-"Default event location"}

print_help(){
    printf "DESCRIPTION:\n"
    printf "\t${COLOR_YEL}Call remote procedure AddEvent${COLOR_END}\n"
    printf "\t${COLOR_YEL}The script uses the https://github.com/ktr0731/evans library${COLOR_END}\n"
    printf "\t${COLOR_RED}Use only for testing!${COLOR_END}\n"

    printf "USAGE:\n"
    printf "\t${COLOR_YEL}${SCRIPT_NAME} --ARGUMENT_NAME ARGUMENT_VALUE --ARGUMENT_NAME2 ARGUMENT_VALUE${COLOR_END}\n"

    printf "ARGUMENTS:\n"
    printf "\t${COLOR_BLU}--start${COLOR_END} ${COLOR_RED}(required)${COLOR_END} event start date e.g.:'2022-04-15T12:30:15.01Z'\n"
    printf "\t${COLOR_BLU}--duration${COLOR_END} event duration in seconds e.g.:3600\n"
    printf "\t${COLOR_BLU}--title${COLOR_END} event title\n"
    printf "\t${COLOR_BLU}--description${COLOR_END} event description\n"
    printf "\t${COLOR_BLU}--location${COLOR_END} event location\n"

    printf "EXAMPLE:\n"
    printf "\t${COLOR_YEL}${SCRIPT_NAME} --start '2022-04-15T12:30:15.01Z' --duration 3600 --title 'example'${COLOR_END}\n"
}

if [[ ! -z ${help} ]]; then
    print_help
else
    if [[ -z ${start} ]]; then
        print_help
        printf "\n${COLOR_RED} required parameter not specified (--start)${COLOR_END}\n"
        exit 1
    fi

    printf '{
      "title": "%s",
      "description": "%s",
      "location": "%s",
      "start_date": "%s",
      "duration": "%s"
    }' "${title}" "${description}" "${location}" "${start}" "${duration}s" | evans cli --call AddEvent
fi
