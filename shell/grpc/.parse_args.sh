#!/usr/bin/env bash

while [[ $# -gt 0 ]]; do
   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare ${param}="$2"
        # echo $1 $2 // Optional to see the parameter:value result
        if [[ $1 == "--help" ]]; then
             declare help="help"
        fi
   fi
  shift
done
