#!/usr/bin/env bash

COLOR_RED="\e[1;31m"
COLOR_GRN="\e[1;32m"
COLOR_YEL="\e[1;33m"
COLOR_BLU="\e[1;34m"
COLOR_MAG="\e[1;35m"
COLOR_CYN="\e[1;36m"
COLOR_END="\e[0m"