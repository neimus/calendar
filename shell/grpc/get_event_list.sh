#!/usr/bin/env bash

PATH_CURRENT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${PATH_CURRENT}/.color.sh
source ${PATH_CURRENT}/.parse_args.sh

printf "${COLOR_RED}Calling this method does not work yet :(${COLOR_END}\n \
${COLOR_YEL}Use REPL mode of evans utility${COLOR_END}\n\
Created issue https://github.com/ktr0731/evans/issues/265\n"
exit 0

SCRIPT_NAME=$(basename "${BASH_SOURCE[0]}")

#start=${start:-"2022-04-15T12:30:15.01Z"}
period=${period:-"day"}

print_help(){
    printf "DESCRIPTION:\n"
    printf "\t${COLOR_YEL}Call remote procedure GetDailyEventList | GetWeeklyEventList | GetMonthlyEventListv${COLOR_END}\n"
    printf "\t${COLOR_YEL}The script uses the https://github.com/ktr0731/evans library${COLOR_END}\n"
    printf "\t${COLOR_RED}Use only for testing!${COLOR_END}\n"

    printf "USAGE:\n"
    printf "\t${COLOR_YEL}${SCRIPT_NAME} --ARGUMENT_NAME ARGUMENT_VALUE --ARGUMENT_NAME2 ARGUMENT_VALUE${COLOR_END}\n"

    printf "ARGUMENTS:\n"
    printf "\t${COLOR_BLU}--start${COLOR_END} ${COLOR_RED}(required)${COLOR_END} event start date e.g.:'2022-04-15T12:30:15.01Z'\n"
    printf "\t${COLOR_BLU}--period${COLOR_END} time period, possible options for day (or d), week (or w), month (or m))\n"

    printf "EXAMPLE:\n"
    printf "\t${COLOR_YEL}${SCRIPT_NAME} --start '2022-04-15T12:30:15.01Z' --period day${COLOR_END}\n"
    printf "\t${COLOR_YEL}${SCRIPT_NAME} --start '2022-04-15T12:30:15.01Z' --period d${COLOR_END}\n"
}

if [[ ! -z ${help} ]]; then
    print_help
else
    if [[ -z ${start} ]]; then
        print_help
        printf "\n${COLOR_RED} required parameter not specified (--start)${COLOR_END}\n"
        exit 1
    fi

    case ${period} in
    "daily"|"day"|"d")
        RPC_METHOD="GetDailyEventList"
       ;;
    "weekly"|"week"|"w")
        RPC_METHOD="GetWeeklyEventList"
       ;;
    "monthly"|"month"|"m")
        RPC_METHOD="GetMonthlyEventList"
       ;;
       *)
        RPC_METHOD="GetDailyEventList"
    esac

    printf '{
      "2022-04-15T12:30:15.01Z"
    }' | evans cli --call ${RPC_METHOD}
fi

printf '{"2022-04-15T12:30:15.01Z"}' | evans cli --call GetDailyEventList
