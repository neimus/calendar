#!/usr/bin/env bash

PATH_CURRENT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${PATH_CURRENT}/.color.sh

SCRIPT_NAME=$(basename "${BASH_SOURCE[0]}")
EVENT_ID=$1

print_help(){
    printf "DESCRIPTION:\n"
    printf "\t${COLOR_YEL}Call remote procedure RemoveEvent${COLOR_END}\n"
    printf "\t${COLOR_YEL}The script uses the https://github.com/ktr0731/evans library for GRPC${COLOR_END}\n"
    printf "\t${COLOR_RED}Use only for testing!${COLOR_END}\n"

    printf "USAGE:\n"
    printf "\t${COLOR_YEL}${SCRIPT_NAME} EVENT_ID${COLOR_END}\n"

    printf "EXAMPLE:\n"
    printf "\t${COLOR_YEL}${SCRIPT_NAME} 85${COLOR_END}\n"
}

if [[ -z ${EVENT_ID} ]]; then
    print_help
    printf "\n${COLOR_RED} event id not specified ${COLOR_END}\n"
else
    printf '{"id": %s}' "${EVENT_ID}" | evans cli --call RemoveEvent
fi
