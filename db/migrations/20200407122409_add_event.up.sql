CREATE TABLE IF NOT EXISTS events
(
    id          SERIAL,
    title       VARCHAR(255)                NOT NULL,
    description TEXT,
    location    VARCHAR(50),
    start_date  TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    duration    INT                         NOT NULL,
    created_at  TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMP WITHOUT TIME ZONE          DEFAULT NULL,
    deleted_at  TIMESTAMP WITHOUT TIME ZONE          DEFAULT NULL
);

CREATE INDEX IF NOT EXISTS idx_start_date
    ON events USING btree (date(start_date))
    WHERE deleted_at IS NULL;
CREATE UNIQUE INDEX IF NOT EXISTS uidx_title_location_start_date
    ON events USING btree (title, location, start_date)
    WHERE deleted_at IS NULL;
