// Package configuration - loads configuration for application
package configuration

import (
	"errors"
	"fmt"
	"path/filepath"

	"github.com/spf13/viper"
)

const (
	defaultLogLevel    = "info"
	defaultLogEncoding = "json"
	defaultTimezone    = "Europe/Moscow"
)

var (
	errConfigurationFile = errors.New("invalid configuration file")
	errConfigArgRequired = errors.New("config argument is required")
)

//New - create new application settings
func New(configFile string) (*viper.Viper, error) {
	if configFile == "" {
		return nil, errConfigArgRequired
	}
	file, err := filepath.Abs(configFile)
	if err != nil {
		return nil, fmt.Errorf("%w: %s", errConfigurationFile, err)
	}

	config := viper.New()
	config.SetConfigFile(file)
	config.SetConfigType("yml")

	config.SetDefault("log.level", defaultLogLevel)
	config.SetDefault("log.encoder", defaultLogEncoding)
	config.SetDefault("app.timezone", defaultTimezone)

	if err := config.ReadInConfig(); err != nil {
		return nil, err
	}

	return config, nil
}
