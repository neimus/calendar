// Package entity - event entity
package entity

import (
	"errors"
	"time"
)

// Event entity
type Event struct {
	id          uint64
	Title       string
	Description string
	Location    string
	StartDate   time.Time
	Duration    time.Duration
}

// ErrChangedID - identifier entity is already set, it cannot be changed
var ErrChangedID = errors.New("identifier entity is already set, it cannot be changed")

// SetID - sets the ID for the event
func (e *Event) SetID(id uint64) error {
	if e.id != 0 {
		return ErrChangedID
	}

	e.id = id
	return nil
}

// ID - return id event
func (e *Event) ID() uint64 {
	return e.id
}
