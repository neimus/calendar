// Package handler - hello world handler
package handler

import (
	"net/http"

	"gitlab.com/neimus/calendar/internal/middleware"
	"go.uber.org/zap"
)

type (
	//HelloHandler - handler that prints "hello world!"
	HelloHandler struct {
		logger *zap.Logger
	}
)

// NewHelloHandler construct
func NewHelloHandler(logger *zap.Logger) *HelloHandler {
	return &HelloHandler{logger}
}

// Routes for hello world
func (h *HelloHandler) Routes() map[string]http.HandlerFunc {
	allowOnlyGet := middleware.NewValidator(h.logger, middleware.Rules{RequestMethod: "GET"})

	return map[string]http.HandlerFunc{
		"/hello": allowOnlyGet.WrapHTTPHandle(h.HandleHello),
	}
}

// HandleHello handler for hello world
func (h *HelloHandler) HandleHello(rw http.ResponseWriter, req *http.Request) {
	count, err := rw.Write([]byte("Hello world!"))
	if err != nil {
		h.logger.Error("Error sending response", zap.Error(err))
	}

	h.logger.Debug("sending message", zap.Int("count", count))
}
