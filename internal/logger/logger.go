//Package logger message logging based on zap.Logger
package logger

import (
	"errors"
	"fmt"
	"log"
	"path/filepath"

	"github.com/coreos/etcd/pkg/fileutil"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var errLogPath = errors.New("log file is not specified or it is incorrect")

//New construct for logger
func New(config *viper.Viper) (*zap.Logger, error) {
	var (
		encodeConfig zapcore.EncoderConfig
		err          error
	)
	outputPaths := []string{"stdout"}
	errOutputPaths := []string{"stderr"}

	if logPath, err := filepath.Abs(config.GetString("log.path")); err != nil {
		fmt.Printf("%s: %s", errLogPath, err)
	} else {
		if err := makeLogPath(logPath); err != nil {
			return nil, err
		}
		outputPaths = append(outputPaths, logPath)
		errOutputPaths = append(errOutputPaths, logPath)
	}

	logLevel := config.GetString("log.level")
	var level zap.AtomicLevel
	if len(logLevel) > 0 {
		if err = level.UnmarshalText([]byte(logLevel)); err != nil {
			return nil, err
		}
	}

	development := config.GetBool("log.development")
	encodeConfig = zap.NewDevelopmentEncoderConfig()
	if !development {
		encodeConfig = zap.NewProductionEncoderConfig()
	}

	logConfig := zap.Config{
		Level:             level,
		Development:       development,
		Encoding:          config.GetString("log.encoder"),
		EncoderConfig:     encodeConfig,
		OutputPaths:       outputPaths,
		ErrorOutputPaths:  errOutputPaths,
		DisableCaller:     !development,
		DisableStacktrace: !development,
	}

	return logConfig.Build()
}

//Flush - flushes buffered application logs to disk
func Flush(logger *zap.Logger) {
	err := logger.Sync()
	if err != nil {
		log.Fatalf("error flush logs to disk: %s", err)
	}
}

func makeLogPath(logPath string) error {
	directory := filepath.Dir(logPath)
	if !fileutil.Exist(directory) {
		err := fileutil.TouchDirAll(directory)
		if err != nil {
			return err
		}
	}
	return nil
}
