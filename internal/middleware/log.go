// Package middleware for logging requests
package middleware

import (
	"context"
	"encoding/json"
	"net/http"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type (
	//Log middleware
	Log struct {
		logger *zap.Logger
	}
)

//NewLog - construct middleware Log
func NewLog(logger *zap.Logger) *Log {
	return &Log{logger: logger}
}

//WrapHTTPHandle wraps the handler for logging
func (l *Log) WrapHTTPHandle(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		l.logger.Info("incoming request",
			zap.String("remote_addr", req.RemoteAddr),
			zap.String("method", req.Method),
			zap.String("request_uri", req.RequestURI),
		)
		handlerFunc(rw, req)
	}
}

//WrapGRPCHandle wraps the interceptor for logging
func (l *Log) WrapGRPCHandle(next grpc.UnaryServerInterceptor) grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (interface{}, error) {

		request, err := json.Marshal(req)
		if err != nil {
			l.logger.Warn("an error occurred while parsing an incoming message to log a request",
				zap.String("method", info.FullMethod),
				zap.Error(err),
			)
		} else {
			l.logger.Info("incoming request",
				zap.String("method", info.FullMethod),
				zap.String("request", string(request)),
			)
		}

		resp, respErr := next(ctx, req, info, handler)
		if statusErr, ok := status.FromError(respErr); ok && statusErr.Code() != codes.OK {
			switch statusErr.Code() {
			case codes.FailedPrecondition, codes.Aborted, codes.OutOfRange,
				codes.Unimplemented, codes.Internal, codes.Unavailable, codes.DataLoss:
				l.logger.Error("critical error in response to request",
					zap.Uint32("code", uint32(statusErr.Code())),
					zap.Error(respErr),
					zap.String("method", info.FullMethod),
					zap.String("request", string(request)),
				)
			default:
				l.logger.Warn("an error occurred while responding to the request",
					zap.Uint32("code", uint32(statusErr.Code())),
					zap.Error(respErr),
					zap.String("method", info.FullMethod),
					zap.String("request", string(request)),
				)
			}
		}

		if response, err := json.Marshal(resp); err != nil {
			l.logger.Debug("an error occurred while parsing an outgoing message to log a response",
				zap.String("method", info.FullMethod),
				zap.Error(err),
			)
		} else {
			l.logger.Debug("outgoing response",
				zap.String("method", info.FullMethod),
				zap.String("response", string(response)),
			)
		}

		return resp, respErr
	}
}
