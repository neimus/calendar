//Package middleware - base middleware
package middleware

import (
	"net/http"

	"google.golang.org/grpc"
)

//HTTPMiddleware interface
type HTTPMiddleware interface {
	WrapHTTPHandle(http.HandlerFunc) http.HandlerFunc
}

//GRPCMiddleware interface
type GRPCMiddleware interface {
	WrapGRPCHandle(grpc.UnaryServerInterceptor) grpc.UnaryServerInterceptor
}
