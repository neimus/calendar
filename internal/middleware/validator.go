//Package middleware for validating incoming messages
package middleware

import (
	"net/http"

	"go.uber.org/zap"
)

type (
	//Rules validation
	Rules struct {
		RequestMethod string
	}

	//Validator middleware for validating incoming messages
	Validator struct {
		logger *zap.Logger
		rules  Rules
	}
)

//NewValidator construct
func NewValidator(logger *zap.Logger, rules Rules) *Validator {
	return &Validator{logger: logger, rules: rules}
}

//WrapHTTPHandle wraps the handler for Validator
func (v *Validator) WrapHTTPHandle(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		if v.rules.RequestMethod != "" && v.rules.RequestMethod != req.Method {
			v.logger.Warn("attempt to access http using not allowed method", zap.String("not_allowed_method", req.Method))
			rw.WriteHeader(405)
			return
		}
		handlerFunc(rw, req)
	}
}
