//Package api for calendar service
package api

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/neimus/calendar/internal/entity"
	"gitlab.com/neimus/calendar/internal/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

//timeout - request execution time
const timeout = 2 * time.Second

//ErrFailedConvertEvent - error when converting an event when responding or requesting
var ErrFailedConvertEvent = errors.New("failed to convert event")

//CalendarService remote procedure call for calendar service
type CalendarService struct {
	eventService service.EventService
}

//NewCalendarService construct
func NewCalendarService(eventService service.EventService) *CalendarService {
	return &CalendarService{eventService: eventService}
}

//AddEvent adding calendar events
func (s *CalendarService) AddEvent(ctx context.Context, req *Event) (*EventID, error) {
	event, eventID, err := s.requestToEvent(req)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	reqCtx, cancelFunc := context.WithTimeout(ctx, timeout)
	defer cancelFunc()

	eventID.Id, err = s.eventService.AddEvent(reqCtx, event)

	if err != nil {
		var errCode codes.Code
		switch err {
		case service.ErrAlreadyExists:
			errCode = codes.AlreadyExists
		case service.ErrInvalidDuration, service.ErrInvalidStartData, service.ErrEmptyTitle:
			errCode = codes.InvalidArgument
		default:
			errCode = codes.Internal
		}
		return nil, status.Error(errCode, err.Error())
	}

	return eventID, nil
}

//UpdateEvent updating calendar events
func (s *CalendarService) UpdateEvent(ctx context.Context, req *Event) (*Event, error) {
	event, eventID, err := s.requestToEvent(req)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := event.SetID(eventID.Id); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	reqCtx, cancelFunc := context.WithTimeout(ctx, timeout)
	defer cancelFunc()

	err = s.eventService.UpdateEvent(reqCtx, event)
	if err != nil {
		var errCode codes.Code
		switch err {
		case service.ErrAlreadyExists:
			errCode = codes.AlreadyExists
		case service.ErrInvalidDuration, service.ErrInvalidStartData, service.ErrEmptyTitle, service.ErrNoIdentifier:
			errCode = codes.InvalidArgument
		default:
			errCode = codes.Internal
		}
		return nil, status.Error(errCode, err.Error())
	}

	return s.eventToResponse(event)
}

//RemoveEvent removing calendar events
func (s *CalendarService) RemoveEvent(ctx context.Context, req *EventID) (*empty.Empty, error) {
	if req.Id == 0 {
		return nil, status.Error(codes.InvalidArgument, service.ErrNoIdentifier.Error())
	}
	reqCtx, cancelFunc := context.WithTimeout(ctx, timeout)
	defer cancelFunc()

	if err := s.eventService.RemoveEvent(reqCtx, req.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return &empty.Empty{}, nil
}

//GetDailyEventList - returns all events in 24 hours from the requested date
func (s *CalendarService) GetDailyEventList(ctx context.Context, req *timestamp.Timestamp) (*EventList, error) {
	reqCtx, cancelFunc := context.WithTimeout(ctx, timeout)
	defer cancelFunc()

	return s.getEventsFromData(reqCtx, req, 24*time.Hour)
}

//GetWeeklyEventList returns all events within a week from the requested date
func (s *CalendarService) GetWeeklyEventList(ctx context.Context, req *timestamp.Timestamp) (*EventList, error) {
	reqCtx, cancelFunc := context.WithTimeout(ctx, timeout)
	defer cancelFunc()

	return s.getEventsFromData(reqCtx, req, 24*time.Hour*7)
}

//GetMonthlyEventList returns all events within a month from the requested date
func (s *CalendarService) GetMonthlyEventList(ctx context.Context, req *timestamp.Timestamp) (*EventList, error) {
	reqCtx, cancelFunc := context.WithTimeout(ctx, timeout)
	defer cancelFunc()

	return s.getEventsFromData(reqCtx, req, 24*time.Hour*30)
}

//RegisterService register grpc service
func (s *CalendarService) RegisterService(server *grpc.Server) {
	RegisterCalendarServer(server, s)
}

func (s *CalendarService) getEventsFromData(
	ctx context.Context,
	reqFromDate *timestamp.Timestamp,
	duration time.Duration) (*EventList, error) {

	fromDate, err := ptypes.Timestamp(reqFromDate)
	if err != nil {
		return nil, err
	}
	events, err := s.eventService.GetEventsFromData(ctx, fromDate, duration)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	eventList := make(map[uint64]*Event, len(events))
	for _, event := range events {
		resp, err := s.eventToResponse(event)
		if err != nil {
			return nil, err
		}
		eventList[event.ID()] = resp
	}

	return &EventList{Events: eventList}, nil
}

func (s *CalendarService) requestToEvent(reqEvent *Event) (entity.Event, *EventID, error) {
	var event entity.Event

	startDate, err := ptypes.Timestamp(reqEvent.StartDate)
	if err != nil {
		return event, nil, fmt.Errorf("%w: %s", service.ErrInvalidStartData, err)
	}

	duration, err := ptypes.Duration(reqEvent.Duration)
	if err != nil {
		return event, nil, fmt.Errorf("%w: %s", service.ErrInvalidDuration, err)
	}

	event.Title = reqEvent.Title
	event.Description = reqEvent.Description
	event.Location = reqEvent.Location
	event.StartDate = startDate
	event.Duration = duration

	eventID := reqEvent.GetId()
	if eventID == nil {
		eventID = &EventID{}
	}

	return event, eventID, nil
}

func (s *CalendarService) eventToResponse(event entity.Event) (*Event, error) {
	startDate, err := ptypes.TimestampProto(event.StartDate)
	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Errorf("%w: %s", ErrFailedConvertEvent, "event.StartDate").Error())
	}
	duration := ptypes.DurationProto(event.Duration)

	return &Event{
		Id:          &EventID{Id: event.ID()},
		Title:       event.Title,
		Description: event.Description,
		Location:    event.Location,
		StartDate:   startDate,
		Duration:    duration,
	}, nil
}
