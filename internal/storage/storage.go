// Package storage - data storage interface
package storage

import (
	"context"
	"errors"
	"time"

	"gitlab.com/neimus/calendar/internal/entity"
)

// ErrEventNotFound - event not found in storage
var ErrEventNotFound = errors.New("event not found")

//Storage implements
type Storage interface {
	// AddEvent - add event to storage
	AddEvent(context.Context, entity.Event) (uint64, error)
	// RemoveEvent - remove event to storage
	RemoveEvent(context.Context, uint64) error
	// UpdateEvent - update event to storage
	UpdateEvent(context.Context, entity.Event) error
	// EventByID -  returns event by id
	EventByID(context.Context, uint64) (entity.Event, error)
	// EventsFromDate - returns events starting from date
	EventsFromDate(context.Context, time.Time, time.Duration) ([]entity.Event, error)
	// SearchDuplicateEvent - looks for duplicate events in the storage
	SearchDuplicateEvent(context.Context, entity.Event) (*entity.Event, error)
}
