//Package storage - event store using postgres
package storage

import (
	"context"
	"time"

	"github.com/jackc/pgx/v4"

	"gitlab.com/neimus/calendar/internal/entity"
)

type postgresStorage struct {
	conn *pgx.Conn
}

//NewPostgresStorage construct
func NewPostgresStorage(conn *pgx.Conn) Storage {
	return &postgresStorage{conn: conn}
}

// AddEvent - add event to storage
func (ps *postgresStorage) AddEvent(ctx context.Context, event entity.Event) (uint64, error) {
	sql := `
        INSERT INTO events (title, description, location, start_date, duration) 
        VALUES ($1, $2, $3, $4, $5)
        RETURNING id
`
	var id uint64
	err := ps.conn.QueryRow(ctx, sql,
		event.Title, event.Description, event.Location, event.StartDate, int32(event.Duration.Seconds())).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

// RemoveEvent - remove event to storage
func (ps *postgresStorage) RemoveEvent(ctx context.Context, id uint64) error {
	sql := `
        UPDATE events SET deleted_at = NOW() 
        WHERE id=$1 AND deleted_at IS NULL
`
	tag, err := ps.conn.Exec(ctx, sql, id)
	if err != nil {
		return err
	}

	if tag.RowsAffected() <= 0 {
		return ErrEventNotFound
	}

	return nil
}

// UpdateEvent - update event to storage
func (ps *postgresStorage) UpdateEvent(ctx context.Context, event entity.Event) error {
	sql := `
        UPDATE events SET title=$1, description=$2, location=$3, start_date=$4, duration=$5, updated_at=NOW()
        WHERE id=$6 AND deleted_at IS NULL
`
	tag, err := ps.conn.Exec(ctx, sql,
		event.Title, event.Description, event.Location, event.StartDate, int32(event.Duration.Seconds()),
		event.ID(),
	)
	if err != nil {
		return err
	}

	if tag.RowsAffected() <= 0 {
		return ErrEventNotFound
	}

	return nil
}

// EventByID -  returns event by id
func (ps *postgresStorage) EventByID(ctx context.Context, id uint64) (entity.Event, error) {
	sql := `
        SELECT title, description, location, start_date, duration 
        FROM events 
        WHERE id=$1 AND deleted_at IS NULL
`
	var (
		event          entity.Event
		durationSecond int32
	)

	err := ps.conn.QueryRow(ctx, sql, id).
		Scan(&event.Title, &event.Description, &event.Location, &event.StartDate, &durationSecond)
	if err == pgx.ErrNoRows {
		return event, ErrEventNotFound
	} else if err != nil {
		return event, err
	}

	event.Duration = time.Duration(durationSecond) * time.Second

	return event, nil
}

// EventsFromDate - returns events starting from date
func (ps *postgresStorage) EventsFromDate(
	ctx context.Context, start time.Time, duration time.Duration,
) ([]entity.Event, error) {
	end := start.Add(duration)

	sql := `
        SELECT id, title, description, location, start_date, duration 
        FROM events 
        WHERE start_date BETWEEN $1 AND $2 
          AND deleted_at IS NULL
`
	var (
		events []entity.Event
	)
	rows, err := ps.conn.Query(ctx, sql, start, end)
	if err == pgx.ErrNoRows {
		return nil, ErrEventNotFound
	} else if err != nil {
		return nil, err
	}
	for rows.Next() {
		var (
			id             uint64
			durationSecond int32
			event          entity.Event
		)

		dest := []interface{}{&id, &event.Title, &event.Description, &event.Location, &event.StartDate, &durationSecond}
		if err := rows.Scan(dest...); err != nil {
			return nil, err
		}

		if err := event.SetID(id); err != nil {
			return nil, err
		}
		event.Duration = time.Duration(durationSecond) * time.Second
		events = append(events, event)
	}

	return events, nil
}

// SearchDuplicateEvent - looks for duplicate events in the storage (Title, StartDate and Location) and filter event id
func (ps *postgresStorage) SearchDuplicateEvent(ctx context.Context, event entity.Event) (*entity.Event, error) {
	sql := `
        SELECT id, title, description, location, start_date, duration 
        FROM events 
        WHERE title=$1 AND location=$2 AND start_date=$3 AND deleted_at IS NULL
`
	var (
		sameEvent      entity.Event
		id             uint64
		durationSecond int32
	)

	args := []interface{}{
		event.Title,
		event.Location,
		event.StartDate,
	}
	dest := []interface{}{
		&id, &sameEvent.Title, &sameEvent.Description, &sameEvent.Location, &sameEvent.StartDate, &durationSecond,
	}

	if event.ID() != 0 {
		sql += " AND id!=$4"
		args = append(args, event.ID())
	}

	if err := ps.conn.QueryRow(ctx, sql, args...).Scan(dest...); err == pgx.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	if err := sameEvent.SetID(id); err != nil {
		return nil, err
	}
	sameEvent.Duration = time.Duration(durationSecond) * time.Second

	return &sameEvent, nil
}
