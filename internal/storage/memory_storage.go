// Package storage - in-memory data store
package storage

import (
	"context"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/neimus/calendar/internal/entity"
)

type memoryStorage struct {
	events map[uint64]entity.Event
	lastID uint64
	mx     sync.RWMutex
}

// NewMemoryStorage construct
func NewMemoryStorage() Storage {
	s := &memoryStorage{}
	s.events = make(map[uint64]entity.Event)
	return s
}

// AddEvent - add event to storage
func (m *memoryStorage) AddEvent(ctx context.Context, event entity.Event) (uint64, error) {
	id := atomic.AddUint64(&m.lastID, 1)
	if err := event.SetID(id); err != nil {
		return 0, err
	}
	m.events[id] = event

	return id, nil
}

// UpdateEvent - update event to storage
func (m *memoryStorage) UpdateEvent(ctx context.Context, event entity.Event) error {
	m.mx.Lock()
	defer m.mx.Unlock()

	if _, err := m.EventByID(ctx, event.ID()); err != nil {
		return ErrEventNotFound
	}

	m.events[event.ID()] = event

	return nil
}

// RemoveEvent - remove event to storage
func (m *memoryStorage) RemoveEvent(ctx context.Context, id uint64) error {
	m.mx.Lock()
	defer m.mx.Unlock()

	if _, err := m.EventByID(ctx, id); err != nil {
		return ErrEventNotFound
	}
	delete(m.events, id)

	return nil
}

// SearchDuplicateEvent - looks for duplicate events in the storage (Title, StartDate and Location)
func (m *memoryStorage) SearchDuplicateEvent(ctx context.Context, desiredEvent entity.Event) (*entity.Event, error) {
	for _, event := range m.events {
		if event.Title == desiredEvent.Title &&
			event.StartDate.Equal(desiredEvent.StartDate) &&
			event.Location == desiredEvent.Location {
			return &event, nil
		}
	}

	return nil, nil
}

// EventsFromDate - returns events starting from date
func (m *memoryStorage) EventsFromDate(
	ctx context.Context,
	startDate time.Time,
	durationLimit time.Duration) ([]entity.Event, error) {
	var events []entity.Event
	endDate := startDate.Add(durationLimit)
	for _, event := range m.events {
		if event.StartDate.After(startDate) && event.StartDate.Before(endDate) {
			events = append(events, event)
		}
	}

	return events, nil
}

// EventByID -  returns event by id
func (m *memoryStorage) EventByID(ctx context.Context, id uint64) (entity.Event, error) {
	event, ok := m.events[id]
	if !ok {
		return entity.Event{}, ErrEventNotFound
	}

	return event, nil
}
