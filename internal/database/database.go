//Package database - creates and controls database connections
package database

import (
	"context"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/log/zapadapter"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

const (
	connTimeout  = 2 * time.Second
	closeTimeout = 2 * time.Second

	fallbackLogLevel = pgx.LogLevelInfo
)

//Database layer for connection and control with the database
type Database struct {
	conn       *pgx.Conn
	connString string
	logger     *zap.Logger
	logLevel   string
}

//NewDatabase construct
func NewDatabase(config *viper.Viper, logger *zap.Logger) *Database {
	return &Database{
		connString: config.GetString("app.database.master"),
		logger:     logger,
		logLevel:   config.GetString("log.level"),
	}
}

//Connect to the database
func (db *Database) Connect(ctx context.Context) (*pgx.Conn, error) {
	var (
		err        error
		level      pgx.LogLevel
		connConfig *pgx.ConnConfig
	)
	connCtx, cancelFunc := context.WithTimeout(ctx, connTimeout)
	defer cancelFunc()

	connConfig, err = pgx.ParseConfig(db.connString)
	if err != nil {
		return nil, err
	}

	level, err = pgx.LogLevelFromString(db.logLevel)
	if err != nil {
		level = fallbackLogLevel
	}

	connConfig.Logger = zapadapter.NewLogger(db.logger)
	connConfig.LogLevel = level
	db.conn, err = pgx.ConnectConfig(connCtx, connConfig)

	return db.conn, err
}

//Close connection with database
func (db *Database) Close() {
	if db.conn == nil {
		return
	}

	ctx, cancelFunc := context.WithTimeout(context.Background(), closeTimeout)
	defer cancelFunc()

	err := db.conn.Close(ctx)
	if err != nil {
		db.logger.Error("failed to close database connection correctly", zap.Error(err))
	}
}
