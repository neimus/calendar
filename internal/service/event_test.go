package service_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/neimus/calendar/internal/entity"
	"gitlab.com/neimus/calendar/internal/service"
	"gitlab.com/neimus/calendar/internal/storage"
)

func TestEventService_AddEvent(t *testing.T) {
	timeNow := time.Now()
	eventService := createEventService(t)

	tCases := []struct {
		name          string
		giveEvent     entity.Event
		expectedError error
		expectedID    uint64
	}{
		{
			name: "empty title",
			giveEvent: entity.Event{
				Title:       "",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(10 * time.Minute),
				Duration:    10 * time.Minute,
			},
			expectedError: service.ErrEmptyTitle,
			expectedID:    0,
		},
		{
			name: "invalid start date",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow,
				Duration:    10 * time.Minute,
			},
			expectedError: service.ErrInvalidStartData,
			expectedID:    0,
		},
		{
			name: "invalid duration",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(10 * time.Minute),
				Duration:    0,
			},
			expectedError: service.ErrInvalidDuration,
			expectedID:    0,
		},
		{
			name: "positive case",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(10 * time.Minute),
				Duration:    10 * time.Minute,
			},
			expectedError: nil,
			expectedID:    1,
		},
		{
			name: "duplicate event",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(10 * time.Minute),
				Duration:    10 * time.Minute,
			},
			expectedError: service.ErrAlreadyExists,
			expectedID:    1,
		},
	}

	for _, tCase := range tCases {
		t.Run(tCase.name, func(t *testing.T) {
			id, err := eventService.AddEvent(context.Background(), tCase.giveEvent)
			assert.Equal(t, tCase.expectedID, id)
			assert.Equal(t, tCase.expectedError, err)
		})
	}
}

func TestEventService_UpdateEvent(t *testing.T) {
	timeNow := time.Now()
	defaultDuration := 10 * time.Minute
	event := entity.Event{
		Title:       "OriginalTitle",
		Description: "OriginalDescription",
		Location:    "",
		StartDate:   timeNow.Add(10 * time.Minute),
		Duration:    10 * time.Minute,
	}

	eventService := createEventService(t)
	id, err := eventService.AddEvent(context.Background(), event)
	if err != nil {
		t.Fatalf("Error adding event: %s", err)
	}

	tCases := []struct {
		name          string
		giveEvent     entity.Event
		giveEventID   uint64
		expectedError error
	}{
		{
			name: "empty title",
			giveEvent: entity.Event{
				Title:       "",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(10 * time.Minute),
				Duration:    defaultDuration,
			},
			giveEventID:   id,
			expectedError: service.ErrEmptyTitle,
		},
		{
			name: "incorrect start date (validation should not work)",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow,
				Duration:    defaultDuration,
			},
			giveEventID:   id,
			expectedError: nil,
		},
		{
			name: "invalid duration",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(defaultDuration),
				Duration:    0,
			},
			giveEventID:   id,
			expectedError: service.ErrInvalidDuration,
		},
		{
			name: "event not found",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(defaultDuration),
				Duration:    defaultDuration,
			},
			giveEventID:   123,
			expectedError: storage.ErrEventNotFound,
		},
		{
			name: "positive case",
			giveEvent: entity.Event{
				Title:       "TestTitle",
				Description: "TestDescription",
				Location:    "",
				StartDate:   timeNow.Add(defaultDuration),
				Duration:    defaultDuration,
			},
			giveEventID:   id,
			expectedError: nil,
		},
	}

	for _, tCase := range tCases {
		t.Run(tCase.name, func(t *testing.T) {
			err := tCase.giveEvent.SetID(tCase.giveEventID)
			assert.NoError(t, err)

			err = eventService.UpdateEvent(context.Background(), tCase.giveEvent)
			assert.Equal(t, tCase.expectedError, err)

			if err == nil {
				actualEvent, err := eventService.GetEvent(context.Background(), tCase.giveEventID)
				if err != nil {
					t.Fatalf("Error GetEvent: %s", err)
				}
				assert.Equal(t, tCase.giveEvent, actualEvent)
			}
		})
	}
}

func TestEventService_RemoveEvent(t *testing.T) {
	event := entity.Event{
		Title:       "TestTitle",
		Description: "TestDescription",
		Location:    "",
		StartDate:   time.Now().Add(10 * time.Minute),
		Duration:    10 * time.Minute,
	}

	eventService := createEventService(t)
	id, err := eventService.AddEvent(context.Background(), event)
	if err != nil {
		t.Fatalf("Error adding event: %s", err)
	}

	actualEvent, err := eventService.GetEvent(context.Background(), id)
	if err != nil {
		t.Fatalf("Error GetEvent: %s", err)
	}
	err = event.SetID(id)
	assert.NoError(t, err)

	assert.Equal(t, event, actualEvent)

	err = eventService.RemoveEvent(context.Background(), id)
	assert.NoError(t, err)

	err = eventService.RemoveEvent(context.Background(), id)
	assert.Equal(t, storage.ErrEventNotFound, err)
}

func TestEventService_GetEventsFromData(t *testing.T) {
	timeNow := time.Now()
	defaultDuration := 10 * time.Minute

	eventToday := entity.Event{
		Title:       "TitleToday",
		Description: "TestDescription",
		Location:    "",
		StartDate:   timeNow.Add(1 * time.Minute),
		Duration:    defaultDuration,
	}
	eventTomorrow := entity.Event{
		Title:       "TitleTomorrow",
		Description: "TestDescription",
		Location:    "",
		StartDate:   timeNow.Add(24 * time.Hour),
		Duration:    defaultDuration,
	}

	eventService := createEventService(t)
	eventIDToday, err := eventService.AddEvent(context.Background(), eventToday)
	if err != nil {
		t.Fatalf("Error adding event (today`s event): %s", err)
	}
	eventIDTomorrow, err := eventService.AddEvent(context.Background(), eventTomorrow)
	if err != nil {
		t.Fatalf("Error adding event (tomorrow`s event): %s", err)
	}

	if err := eventToday.SetID(eventIDToday); err != nil {
		t.Fatalf("Error SetID (today`s event): %s", err)
	}

	if err := eventTomorrow.SetID(eventIDTomorrow); err != nil {
		t.Fatalf("Error SetID (tomorrow`s event): %s", err)
	}

	tCases := []struct {
		name           string
		giveStartData  time.Time
		giveDuration   time.Duration
		expectedEvents map[uint64]entity.Event
	}{
		{
			name:          "get only today`s events",
			giveStartData: timeNow,
			giveDuration:  defaultDuration,
			expectedEvents: map[uint64]entity.Event{
				eventIDToday: eventToday,
			},
		},
		{
			name:          "get only tomorrow`s events",
			giveStartData: timeNow.Add(24 * time.Hour).Truncate(1 * time.Minute),
			giveDuration:  defaultDuration,
			expectedEvents: map[uint64]entity.Event{
				eventIDTomorrow: eventTomorrow,
			},
		},
		{
			name:          "get all events",
			giveStartData: timeNow,
			giveDuration:  25 * time.Hour,
			expectedEvents: map[uint64]entity.Event{
				eventIDToday:    eventToday,
				eventIDTomorrow: eventTomorrow,
			},
		},
	}

	for _, tCase := range tCases {
		t.Run(tCase.name, func(t *testing.T) {
			events, err := eventService.GetEventsFromData(context.Background(), tCase.giveStartData, tCase.giveDuration)
			assert.NoError(t, err)
			assert.Len(t, events, len(tCase.expectedEvents))
			for _, event := range events {
				if expectedEvent, ok := tCase.expectedEvents[event.ID()]; ok {
					assert.Equal(t, expectedEvent, event)
				} else {
					t.Errorf("\n Expected: %#v\n: Actual%#v", expectedEvent, events)
					break
				}
			}
		})
	}
}

func createEventService(t *testing.T) service.EventService {
	location, err := time.LoadLocation("Europe/Moscow")
	if err != nil {
		t.Fatalf("Unable to set timezone: %s", err)
	}

	eventStorage := storage.NewMemoryStorage()
	return service.NewEventService(eventStorage, location)
}
