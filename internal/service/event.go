//Package service for managing calendar events
package service

import (
	"context"
	"errors"
	"time"

	"gitlab.com/neimus/calendar/internal/entity"
	"gitlab.com/neimus/calendar/internal/storage"
)

type (
	// EventService implements
	EventService interface {
		AddEvent(context.Context, entity.Event) (uint64, error)
		UpdateEvent(context.Context, entity.Event) error
		RemoveEvent(context.Context, uint64) error
		GetEventsFromData(context.Context, time.Time, time.Duration) ([]entity.Event, error)
		GetEvent(context.Context, uint64) (entity.Event, error)
	}

	eventService struct {
		location *time.Location
		storage  storage.Storage
	}
)

// ErrEmptyTitle - error with empty title in event
var ErrEmptyTitle = errors.New("event title is required")

// ErrInvalidStartData - start date is not correct in event
var ErrInvalidStartData = errors.New("start date is not correct")

// ErrInvalidDuration - duration is not correct
var ErrInvalidDuration = errors.New("duration is not correct")

// ErrAlreadyExists - error when event already exists
var ErrAlreadyExists = errors.New("event already exists")

// ErrNoIdentifier - error when event id not set
var ErrNoIdentifier = errors.New("no identifier for event")

// NewEventService construct
func NewEventService(storage storage.Storage, location *time.Location) EventService {
	return &eventService{location: location, storage: storage}
}

// AddEvent - adds and validates an event
func (e *eventService) AddEvent(ctx context.Context, event entity.Event) (uint64, error) {
	if err := e.validateEvent(event); err != nil {
		return 0, err
	}

	// Check if there is already the same event
	sameEvent, err := e.storage.SearchDuplicateEvent(ctx, event)
	if err != nil {
		return 0, err
	}

	if sameEvent != nil {
		return sameEvent.ID(), ErrAlreadyExists
	}

	return e.storage.AddEvent(ctx, event)
}

// UpdateEvent - update and validates an event
func (e *eventService) UpdateEvent(ctx context.Context, event entity.Event) error {
	if event.ID() == 0 {
		return ErrNoIdentifier
	}
	if err := e.validateEvent(event); err != nil {
		return err
	}

	// Check if there is already the same event
	sameEvent, err := e.storage.SearchDuplicateEvent(ctx, event)
	if err != nil {
		return err
	}

	if sameEvent != nil {
		return ErrAlreadyExists
	}

	return e.storage.UpdateEvent(ctx, event)
}

// RemoveEvent - remove event if exists
func (e *eventService) RemoveEvent(ctx context.Context, id uint64) error {
	return e.storage.RemoveEvent(ctx, id)
}

// GetEventsFromData - returns events starting from date
func (e *eventService) GetEventsFromData(
	ctx context.Context,
	startDate time.Time,
	duration time.Duration) ([]entity.Event, error) {
	return e.storage.EventsFromDate(ctx, startDate, duration)
}

// GetEvent -  returns event by id
func (e *eventService) GetEvent(ctx context.Context, id uint64) (entity.Event, error) {
	return e.storage.EventByID(ctx, id)
}

func (e *eventService) validateEvent(event entity.Event) error {
	if event.Title == "" {
		return ErrEmptyTitle
	}

	// Check only for new events
	if event.ID() == 0 && event.StartDate.Before(time.Now()) {
		return ErrInvalidStartData
	}

	if event.Duration.Minutes() < 1 {
		return ErrInvalidDuration
	}

	return nil
}
