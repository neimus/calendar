// Package listener - listens to http requests and runs the appropriate handlers
package listener

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/neimus/calendar/internal/middleware"
	"go.uber.org/zap"
)

type (
	//Handler interface for handlers
	Handler interface {
		Routes() map[string]http.HandlerFunc
	}

	//HTTPListener interface for listens to http requests
	HTTPListener interface {
		AddHandler(Handler) HTTPListener
		AddMiddleware(middleware.HTTPMiddleware) HTTPListener
		Serve(cancelFunc context.CancelFunc)
		Shutdown(timeout time.Duration)
	}

	httpListener struct {
		ip             string
		port           string
		logger         *zap.Logger
		listMiddleware []middleware.HTTPMiddleware
		handlers       []Handler
		server         http.Server
	}
)

// NewHTTPListener construct
func NewHTTPListener(ip, port string, logger *zap.Logger) HTTPListener {
	return &httpListener{ip: ip, port: port, logger: logger}
}

func (l *httpListener) createRoutes() *http.ServeMux {
	mux := http.NewServeMux()
	for _, handler := range l.handlers {
		for pattern, handleFunc := range handler.Routes() {
			handleFunc = l.wrapHandleFunc(handleFunc)
			pattern = strings.Trim(pattern, "/")
			mux.HandleFunc(fmt.Sprintf("/%s", pattern), handleFunc)
			mux.HandleFunc(fmt.Sprintf("/%s/", pattern), handleFunc)
		}
	}

	return mux
}

func (l *httpListener) wrapHandleFunc(handleFunc http.HandlerFunc) http.HandlerFunc {
	for _, m := range l.listMiddleware {
		handleFunc = m.WrapHTTPHandle(handleFunc)
	}

	return handleFunc
}

// AddMiddleware - adds global middleware to which handlers will be wrapped
func (l *httpListener) AddMiddleware(middleware middleware.HTTPMiddleware) HTTPListener {
	l.listMiddleware = append(l.listMiddleware, middleware)

	return l
}

// AddHandler - adds handlers
func (l *httpListener) AddHandler(handler Handler) HTTPListener {
	l.handlers = append(l.handlers, handler)

	return l
}

// Serve - listens to requests and starts processing them
func (l *httpListener) Serve(cancelFunc context.CancelFunc) {
	addr := fmt.Sprintf("%s:%s", l.ip, l.port)
	l.logger.Info("start HTTP server", zap.String("addr", addr))
	l.server = http.Server{
		Addr:    addr,
		Handler: l.createRoutes(),
	}

	err := l.server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		l.logger.Error("listen HTTP serve failed", zap.Error(err))
	}
	cancelFunc()
}

// Shutdown gracefully shuts down the server without interrupting any active connections
func (l *httpListener) Shutdown(timeout time.Duration) {
	l.logger.Info("server shutdown...")
	ctx, cancelFunc := context.WithTimeout(context.Background(), timeout)
	go func() {
		err := l.server.Shutdown(ctx)
		if err != nil {
			l.logger.Error("HTTP server shutdown failed", zap.Error(err))
		}
		cancelFunc()
	}()

	<-ctx.Done()
	cancelFunc()
}
