//Package listener - listens grpc and run grpc server
package listener

import (
	"context"
	"fmt"
	"net"
	"time"

	"gitlab.com/neimus/calendar/internal/middleware"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

//GRPCService implementation grpc service
type GRPCService interface {
	RegisterService(*grpc.Server)
}

//GRPCListener - grpc listener
type GRPCListener struct {
	ip           string
	port         string
	logger       *zap.Logger
	interceptor  grpc.UnaryServerInterceptor
	server       *grpc.Server
	grpcServices []GRPCService
}

//NewGRPCListener construct
func NewGRPCListener(ip string, port string, logger *zap.Logger) *GRPCListener {
	return &GRPCListener{
		ip:          ip,
		port:        port,
		logger:      logger,
		interceptor: createHandleInterceptor(),
	}
}

//AddMiddleware - adds global interceptor to which handlers will be wrapped
func (l *GRPCListener) AddMiddleware(interceptor middleware.GRPCMiddleware) *GRPCListener {
	l.interceptor = interceptor.WrapGRPCHandle(l.interceptor)

	return l
}

//AddService - adds a service to run
func (l *GRPCListener) AddService(service GRPCService) *GRPCListener {
	l.grpcServices = append(l.grpcServices, service)

	return l
}

func (l *GRPCListener) registerServices() {
	for _, service := range l.grpcServices {
		service.RegisterService(l.server)
	}
}

// Serve - listens to requests and starts processing them
func (l *GRPCListener) Serve(cancelFunc context.CancelFunc) {
	if len(l.grpcServices) == 0 {
		l.logger.Error("GRPC server not created. No GRPC service added")
	}

	l.server = grpc.NewServer(grpc.UnaryInterceptor(l.interceptor))

	addr := fmt.Sprintf("%s:%s", l.ip, l.port)
	l.logger.Info("start GRPC server", zap.String("addr", addr))

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		l.logger.Error("listen GRPC serve failed", zap.Error(err))
	}

	l.registerServices()

	if err = l.server.Serve(lis); err != nil {
		l.logger.Error("listen GRPC serve failed", zap.Error(err))
	}
	cancelFunc()
}

// Shutdown stops the gRPC server gracefully
func (l *GRPCListener) Shutdown(timeout time.Duration) {
	l.logger.Info("GRPC server shutdown...")
	ctx, cancelFunc := context.WithTimeout(context.Background(), timeout)
	go func() {
		l.server.GracefulStop()
		cancelFunc()
	}()

	<-ctx.Done()
	cancelFunc()
}

func createHandleInterceptor() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (interface{}, error) {
		return handler(ctx, req)
	}
}
