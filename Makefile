SHELL:=/bin/bash

COLOR_RED := \e[1;31m
COLOR_GRN := \e[1;32m
COLOR_YEL := \e[1;33m
COLOR_BLU := \e[1;34m
COLOR_MAG := \e[1;35m
COLOR_CYN := \e[1;36m
COLOR_END := \e[0m

@print = @printf "${2}${1}${COLOR_END}\n"
print = printf "${2}${1}${COLOR_END}\n"

##? Application version
APP_VERSION ?= $(shell date -u +%Y-%m-%d.%H:%M:%S)

## Current path
PATH_CURRENT = $(shell pwd)

## Migration path
PATH_MIGRATIONS = "$(PATH_CURRENT)/db/migrations"

##? URL для Postgres
DB_URL ?= ""

##? The path where the binary file will be placed
PATH_OUTPUT_BIN ?= $(PATH_CURRENT)/.bin

##? Set the mode for code-coverage
GO_TEST_COVERAGE_MODE ?= count

##? File name for code-coverage
GO_TEST_COVERAGE_FILE_NAME ?= coverage.out

##? Set a default `min_confidence` value for `golint`
GO_LINT_MIN_CONFIDENCE ?= 0.2

##? `golangci-lint` argument
GO_LINT_ARGUMENTS ?= --deadline=5m --enable=dupl --enable=misspell --enable=lll --enable=goconst

all: help

.PHONY: install build run proto compress dependency generate config tools tools-test migrate-up migrate-down
##@ Build
# gcflags and asmflags (trimpath)- remove prefix from recorded source file paths
# ldflags forward the version value to a variable Version
install: generate build config ## Install and build the whole project (get dependencies, copy config and build binaries)
build: dependency ## Build binary files
	$(call @print,Build binaries,${COLOR_BLU})
	@mkdir -p $(PATH_OUTPUT_BIN)
	@ERR=0; \
	for CMD in $$(find "./cmd" -maxdepth 1 -mindepth 1 -type d -print); do \
		BIN=$$(basename "$${CMD}"); \
		go build -gcflags="-trimpath=$(PATH_CURRENT)" \
				-asmflags="-trimpath=$(PATH_CURRENT)" \
				-ldflags "-X main.Version=$(APP_VERSION)" \
				-o "$(PATH_OUTPUT_BIN)/$${BIN}" "$${CMD}" || { \
			ERR=$$?; \
			break; \
		}; \
	done; \
	if [ $$ERR != 0 ]; then \
		exit $$ERR; \
	fi

run: ## Run application
	$(call @print,Run application...,${COLOR_GRN}); \
	cd $(PATH_OUTPUT_BIN) && \
	./calendar --config config.yml

migrate-up: ## The use of migrations for the database
	$(call @print,Migration up...,${COLOR_BLU}); \
	CONFIG="${PATH_OUTPUT_BIN}/config.yml"; \
	if [[ -f $$CONFIG ]]; then \
		. $(PATH_CURRENT)/shell/parse_yml.sh; \
		eval $$(parse_yaml "${PATH_OUTPUT_BIN}/config.yml" "config_"); \
		if [ -z "$$config_app_database_master" ]; then \
			$(call print,The database connection is not specified in the configuration file (app.database.master),${COLOR_RED}); \
		else \
		  	migrate -database $$config_app_database_master -path $(PATH_MIGRATIONS) up; \
		fi; \
	else \
		$(call print,No $$CONFIG configuration file found,${COLOR_RED}); \
	fi

migrate-down: ## Rollback migration for the database
	$(call @print,Migration down...,${COLOR_BLU}); \
	CONFIG="${PATH_OUTPUT_BIN}/config.yml"; \
	if [[ -f $$CONFIG ]]; then \
		. $(PATH_CURRENT)/shell/parse_yml.sh; \
		eval $$(parse_yaml "${PATH_OUTPUT_BIN}/config.yml" "config_"); \
		if [ -z "$$config_app_database_master" ]; then \
			$(call print,The database connection is not specified in the configuration file (app.database.master),${COLOR_RED}); \
		else \
		  	migrate -database $$config_app_database_master -path $(PATH_MIGRATIONS) down; \
		fi; \
	else \
		$(call print,No $$CONFIG configuration file found,${COLOR_RED}); \
	fi

proto: ## Generating code from protobuf files
	$(call @print,Generating code from protobuf files...,${COLOR_BLU})
	@ERR=0; \
    for PATHPB in $$(find "./proto" -maxdepth 1 -mindepth 1 -type d -print); do \
		for FILEPB in "$${PATHPB}"/*; do \
			PKG=$$(basename "$${PATHPB}"); \
			OUTPUT=$(PATH_CURRENT)/internal/$${PKG}; \
			mkdir -p $${OUTPUT} && \
			$(call print,$${FILEPB} -> $${OUTPUT},${COLOR_YEL}); \
			protoc -I=$${PATHPB} --go_out=plugins=grpc:$${OUTPUT} $${FILEPB} || { \
				ERR=$$?; \
				break; \
		}; \
		done; \
	done; \
	if [ $$ERR != 0 ]; then \
		$(call print,Error generating code from protobuf files...,${COLOR_RED});  \
		$(call print,Install the protoc,${COLOR_RED}); \
		exit $$ERR; \
	fi;

compress: ## Compress binaries
	$(call @print,Compress binaries,${COLOR_BLU})
	@ERR=0; \
	for CMD in $$(find "./cmd" -maxdepth 1 -mindepth 1 -type d -print); do \
		BIN=$$(basename "$${CMD}"); \
		upx --best -q "$(PATH_OUTPUT_BIN)/$${BIN}" || { \
			ERR=$$?; \
			break; \
		}; \
	done; \
	if [ $$ERR != 0 ]; then \
		$(call print,The compression is not made,${COLOR_RED}); \
		$(call print,Install the upx package,${COLOR_RED}); \
		exit $$ERR; \
	fi;
dependency: ## Download all dependencies
	$(call @print,Download all dependencies,${COLOR_BLU})
	@ERR=0 && \
	go mod download && \
	ERR=$$? && \
	if [ $$ERR != 0 ]; then \
		exit $$ERR; \
	fi
generate: ## Generation code
	$(call @print,Generate code files,${COLOR_BLU})
	@go generate ./... || exit 1
config: ## Cope and replace configs
	@CONFIG="$(PATH_CURRENT)/config/config.dist.yml"; \
	if [[ -f $$CONFIG ]]; then \
		$(call print,Configs replace,${COLOR_BLU}); \
		cp -R "$(PATH_CURRENT)/config/config.dist.yml" "${PATH_OUTPUT_BIN}/config.yml"; \
	fi
tools: ## Install all tools for project
	@export GO111MODULE=off && \
	export GOFLAGS="" && \
	echo "Install golint"; \
    go get -u -t golang.org/x/lint/golint; \
    echo "Install goimports"; \
    go get -u -t golang.org/x/tools/cmd/goimports; \
    echo "Install golangci-lint"; \
    go get -u github.com/golangci/golangci-lint/cmd/golangci-lint; \
    echo "Install gosec"; \
    go get -u github.com/securego/gosec/cmd/gosec; \
	echo "Install mockery"; \
	go get -u github.com/vektra/mockery/.../; \
	echo "Install migrate"; \
	go get -tags 'postgres' -u github.com/golang-migrate/migrate/cmd/migrate;
tools-test:
	@export GO111MODULE=off && \
	export GOFLAGS="" && \
	echo "Install go-junit-report"; \
	go get -u github.com/jstemmer/go-junit-report

.PHONY: test test-unit test-with-coverage test-race test-coverage-gitlab-ci test-gitlab-ci
##@ Test
test: test-unit test-with-coverage test-race ## Run test and show coverage
test-unit:
	$(call @print,Run unit tests,${COLOR_BLU})
	@go test -v ./...
test-with-coverage:
	$(call @print,Run unit tests with coverage,${COLOR_BLU})
	@go test -cover ./...
test-race:
	$(call @print,Run race detection,${COLOR_BLU})
	@go test -race  ./...
test-coverage-gitlab-ci:
	$(call @print,Run unit tests with coverage profile,${COLOR_BLU})
	@echo "mode: ${GO_TEST_COVERAGE_MODE}" > "${GO_TEST_COVERAGE_FILE_NAME}"; \
	go test -coverpkg=`go list ./... | grep -vE 'command|domain' | tr '\n' ','` -covermode ${GO_TEST_COVERAGE_MODE} -coverprofile=${GO_TEST_COVERAGE_FILE_NAME} ./... && \
	echo "Generate coverage report"; \
	go tool cover -func="${GO_TEST_COVERAGE_FILE_NAME}" && \
	rm "${GO_TEST_COVERAGE_FILE_NAME}";
test-gitlab-ci:
	@go test -v ./... 2>&1 | go-junit-report

.PHONY: lint lint-format lint-style lint-ci lint-gosec
##@ Linter
lint: lint-format lint-ci lint-style lint-gosec ## Run code analysis (Check formatting, code style and check code with GolangCI-Lint )
lint-format: ## Check formatting
	$(call @print,Check formatting,${COLOR_BLU})
	@export GO111MODULE="off" && \
	export GOFLAGS="" && \
	errors=$$(gofmt -l ${GO_FMT_FLAGS} $$(go list -f "{{ .Dir }}" ./...)); \
	if [[ "$${errors}" != "" ]]; then echo "$${errors}"; exit 1; fi
lint-style: ## Check code style
	$(call @print,Check code style,${COLOR_BLU})
	@golint -set_exit_status=1 -min_confidence=${GO_LINT_MIN_CONFIDENCE} $$(go list ./...)
lint-ci: ## Check code with GolangCI-Lint
	$(call @print,Check code with GolangCI-Lint,${COLOR_BLU})
	@golangci-lint $(GO_LINT_ARGUMENTS) run
lint-gosec:
	$(call @print,Analisys code with gosec,${COLOR_BLU})
	@gosec -tests ./...

.PHONY: clean-all clean remove-dependency tidy mod-init migrate-create
##@ Tools
clean-all: clean remove-dependency ## Cleanup all (clear cache, remove dependencies)
	$(call @print,Cleanup all,${COLOR_BLU})
clean: ## Delete temporary files
	$(call @print,Clear cache,${COLOR_BLU})
	@find . -type f -name "*easyjson*" -delete
	@find . -type f -name "*coverage*.out" -delete
remove-dependency: ## Remove folder `GOPATH/pkg/mod/`
	$(call @print,Remove dependency,${COLOR_BLU})
	@sudo rm -rf $(GOPATH)/pkg/mod/
tidy: ## alias go mod tidy
	@go mod tidy
migrate-create:
	@read -p "Enter migration name : " MIGRATION_NAME; \
	ERR=0; \
	mkdir -p $(PATH_MIGRATION) && \
	migrate create -ext sql -dir $(PATH_MIGRATION) $$MIGRATION_NAME || { \
		ERR=$$?; \
		break; \
	}; \
	if [ $$ERR != 0 ]; then \
		$(call print,failed to create migration ${MIGRATION_NAME},${COLOR_RED}); \
    	exit $$ERR; \
    fi; \
	$(call print,${MIGRATION_NAME} migration created successfully,${COLOR_GRN})

.PHONY: help
help:
	@awk 'BEGIN {RS = ""; FS="\n"; printf "\nVariables:\n"; } /^##\?.*\n[a-zA-Z_-]+/ { sub(/\?=.*/, "", $$2); printf "  \033[36m%-15s\033[0m %s\n", $$2, substr($$1, 5) }' $(MAKEFILE_LIST)
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
