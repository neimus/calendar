# Календарь

### Установка
```bash
export GO111MODULE=on && go get -u gitlab.com/neimus/calendar
```

### Запуск
1. Запустите команду `make install` -> установит все зависимости, скомпилирует бинарный файл в папку `.bin` и создаст файл `config.yml`
2. Сконфигурируйте файл `.bin/config.yml` (если необходимо)
3. Запустите бинарный файл 
```bash
cd .bin && ./calendar --config config.yml
```
или 
```bash
make run
```

### База данных и миграции (для разработки)
1. Для запуска postgres в docker используйте команду:
`docker-compose up -d`
2. Для применения миграций:
```bash
make migrate-up
```
3. Для откатывание миграций:
```bash
make migrate-down
```
4. Для остановки postgres используйте команду:
`docker-compose down`

### Для тестировани создан клиент (bash-скрипты):
Скрипты основаны на утилите evans, для их работы необходимо установить https://github.com/ktr0731/evans
Все скрипты поддерживают отображение справки `--help`
 * `shell/grpc/add_event.sh` - добаляет событие
 * `shell/grpc/update_event.sh` - обновляет событие по id
 * `shell/grpc/remove_event.sh` - удаляет событие по id
 * `shell/grpc/get_event_list.sh` - возвращает события (пока не работает, для тестирования можно воспрльзоватся REPL режимом утилиты evans)

### Основные make-команды:
 * `make install` - установка всех зависимостей, создания бинарников
 * `make run` - запуск скомпилированного бинарника (используетй только после `make install`)
 * `make dependency` - устанавливает все зависимости в `$GOPATH/pkg/mod`
 * `make compress` - компрессия бинарников (требуется `upx`)
 * `make tools` - установка инструментов для разработки и не только (linter, migrate ...)
 * `make test` - запуск тестов и проверка покрытия кода тестами
 * `make lint` - запуск проверки code style и ошибок в коде (перед запуском необходимо установить все инструменты `make tools`)
 * `make clean-all` - удаляет файлы созданные easyjson, coverage и очищает все зависимости `$GOPATH/pkg/mod`
 * `make tidy` - алиас для `go mod tidy`
 * `make migrate-create` - создает файлы для миграций
 * `make proto` - генерация proto-файлов
 * `make` или `make help` - вызов справки по командам