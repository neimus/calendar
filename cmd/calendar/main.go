// Package main provide component entry point.
package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/neimus/calendar/internal/api"
	"gitlab.com/neimus/calendar/internal/database"
	"gitlab.com/neimus/calendar/internal/service"
	"gitlab.com/neimus/calendar/internal/storage"

	"gitlab.com/neimus/calendar/internal/configuration"
	"gitlab.com/neimus/calendar/internal/handler"
	"gitlab.com/neimus/calendar/internal/listener"
	"gitlab.com/neimus/calendar/internal/logger"
	"gitlab.com/neimus/calendar/internal/middleware"
	"go.uber.org/zap"
)

// Version is component version (set when compiling the application)
const Version = "0.0.1"

const shutdownTimeout = 2 * time.Second

var (
	configFile string
)

func init() {
	pflag.StringVar(&configFile, "config", "", "configuration file")
}

func main() {
	pflag.Parse()
	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	config, err := configuration.New(configFile)
	if err != nil {
		log.Fatal(err)
	}

	zLogger, err := logger.New(config)
	if err != nil {
		log.Fatal(err)
	}
	defer logger.Flush(zLogger)

	location, err := time.LoadLocation(config.GetString("app.timezone"))
	if err != nil {
		zLogger.Error("Unable to set timezone", zap.Error(err))
		os.Exit(2)
	}

	var conn *pgx.Conn
	db := database.NewDatabase(config, zLogger)
	conn, err = db.Connect(ctx)
	if err != nil {
		zLogger.Error("Unable to connect to database", zap.Error(err))
		os.Exit(3)
	}
	defer db.Close()

	zLogger.Info(fmt.Sprintf("start app, program version %s", Version))

	postgresStorage := storage.NewPostgresStorage(conn)
	eventService := service.NewEventService(postgresStorage, location)

	httpListener := runHTTPServer(cancelFunc, config, zLogger)
	grpcListener := runGRPCServer(cancelFunc, config, zLogger, eventService)

	select {
	case <-interrupt:
		httpListener.Shutdown(shutdownTimeout)
		grpcListener.Shutdown(shutdownTimeout)
		zLogger.Error("program was interrupted by the user")
		os.Exit(1)
	case <-ctx.Done():
	}
}

func runGRPCServer(cancelFunc context.CancelFunc,
	config *viper.Viper,
	zLogger *zap.Logger,
	eventService service.EventService) *listener.GRPCListener {
	grpcListener := listener.NewGRPCListener(
		config.GetString("app.grpc.ip"),
		config.GetString("app.grpc.port"),
		zLogger,
	)

	grpcListener.AddMiddleware(middleware.NewLog(zLogger))
	grpcListener.AddService(api.NewCalendarService(eventService))
	go grpcListener.Serve(cancelFunc)

	return grpcListener
}

func runHTTPServer(cancelFunc context.CancelFunc, config *viper.Viper, zLogger *zap.Logger) listener.HTTPListener {
	httpListener := listener.NewHTTPListener(
		config.GetString("app.http_listen.ip"),
		config.GetString("app.http_listen.port"),
		zLogger,
	)
	httpListener.
		AddMiddleware(middleware.NewLog(zLogger)).
		AddHandler(handler.NewHelloHandler(zLogger))

	go httpListener.Serve(cancelFunc)

	return httpListener
}
